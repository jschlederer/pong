/************************************************************************************
 * This file was developed as part of CS3841 Design of Operating Systems at the
 * Milwaukee School of Engineering.  This file is copyright 2008-2014 by MSOE.
 *
 * $Author: wws $
 * $Revision: 1.1 $
 * $Name:  $
 * This file manages the motion of the oponent's paddle.
 *
 ************************************************************************************/
#define OPPONENT_C

/************************************************************************************
 * External Includes
 ************************************************************************************/
#include "opponent.h"
#include "pong.h"

/************************************************************************************
 * Private structure / type definitions
 ************************************************************************************/
#define PADDLE_SIZE (5)

/************************************************************************************
 * Private function / method prototypes
 ************************************************************************************/

/************************************************************************************
 * Constant declarations / table declarations
 ***********************************************************************************/

/************************************************************************************
 * Method header:
 * Function name: moveopponent
 * Function purpose: This function is responsible for moving the opponents paddle.
 *                   It is spawned as a thread and will exit if and when quit is no longer true.
 * Function parameters: 
 *                   void *vp - This is a pointer to the parameters passed into the 
 *                              thread.  At the present time, this parameter is not used.
 * Function return value: void* This is the return value when the thread exits.  
 *                              Currently, it is always NULL, as no data is directly 
 *                              returned by the thread.
 ************************************************************************************/
void *moveopponent(void* vp) {
	int vpos = 0;	// start at top row
	int vprev;

	// get the extents of the screen
	int maxx;
	int maxy;
	getmaxyx(win, maxy, maxx);

	// draw the default paddle
	int i;
	for (i = 0; i < PADDLE_SIZE; i++) {
		pthread_mutex_lock(&drawlock);
		mvaddch(i, maxx - 1, ' ' | A_REVERSE);
		pthread_mutex_unlock(&drawlock);
	}

	//While not paused or quit the opponent paddle follows the y-axis of ball
	while (!quit) {
		while (paused) {
			usleep(1000);
		}
		//If ball if greater than paddle then paddle moves up
		if (bally > vpos) {
			vprev = vpos;
			vpos = bally;
			if (vpos > (maxy - PADDLE_SIZE)) {
				vpos = (maxy - PADDLE_SIZE);
			}
			if (vpos != vprev) {
				pthread_mutex_lock(&drawlock);
				mvaddch(vpos - 1, maxx - 1, ' ' | A_NORMAL);
				mvaddch(vpos+PADDLE_SIZE-1, maxx - 1, ' ' | A_REVERSE);
				pthread_mutex_unlock(&drawlock);
			}
			//If ball is less than paddle then paddle moves down
		} else if (bally < vpos) {
			vprev = vpos;
			vpos = bally;
			if (vpos < 0) {
				vpos = 0;
			}
			if (vpos != vprev) {
				pthread_mutex_lock(&drawlock);
				mvaddch(vpos, maxx - 1, ' ' | A_REVERSE);
				mvaddch(vpos+PADDLE_SIZE, maxx - 1, ' ' | A_NORMAL);
				pthread_mutex_unlock(&drawlock);
			}
		}

	}
	return NULL;
}

