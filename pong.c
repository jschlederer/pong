/************************************************************************************
 * This file was developed as part of CS3841 Design of Operating Systems at the
 * Milwaukee School of Engineering.  This file is copyright 2008-2014 by MSOE.
 *
 * $Author: wws $
 * $Revision: 1.1 $
 * $Name:  $
 * This file manages serves as the main program for the pong game.
 * It is responsible for spawning all other threads and managing the overall
 * behavior of the game.  All global variables are owned by this file.
 *
 ************************************************************************************/
#define PONG_C

/************************************************************************************
 * External Includes
 ************************************************************************************/
#include <pthread.h>
#include <curses.h>
#include <sys/types.h>
#include <unistd.h>

#include "pong.h"
#include "paddlectrl.h"
#include "ballctrl.h"
#include "opponent.h"
#include "timer.h"
#include "AudioPlayer.h"

/************************************************************************************
 * Method header:
 * Function name: main
 * Function purpose: This is the main function for the program.
 * Function parameters: 
 *                   int argc  - This is a count of the number of command line parameters 
 *                               passed by the operating system.
 *                   char *vp[] - This is the array of strings which is passed to program.
 * Function return value: int.  0 will be returned upon normal exiting of the program.  
 *                                A negative value will indicate an error.
 ************************************************************************************/
int main(int argc, char* argv[]) {
	int rc1;
	int rc2;
	int rc3;
	int rc4;
	int rc5;
	int rc6;

	pthread_t thread1, thread2, thread3, thread4, thread5, thread6;

	//Check to see if all command line arguments are passed in correctly
	if (argc != 4 || (lvlDif = atoi(argv[1])) > 2 || lvlDif < 1 || (volume =
			atoi(argv[2])) > 1 || volume < 0) {
		printf(
				"Usage: ./lab5 <difficulty (1 or 2)> <volume (0 or 1)> <player name>\n");
		return 0;
	}

	//Initialize all global variables
	ballx = 1;
	bally = 1;
	quit = false;
	paused = false;
	playerName = argv[2];
	pthread_mutex_init(&drawlock, NULL);
	pthread_mutex_init(&soundlock, NULL);

	//Initialize and open audio interface
	initializeAudioInterface("plughw:0", 11025, 2, SND_PCM_STREAM_PLAYBACK);
	openAudioInterface();

	// init window - see curses documentation for guidance
	win = initscr();
	timeout(-1);
	cbreak();
	noecho();
	curs_set(0);
	keypad(win, TRUE);
	nodelay(win, TRUE);
	refresh();

	int maxx, maxy;
	getmaxyx(win, maxy, maxx);

	pthread_mutex_lock(&drawlock);

	clear();
	mvprintw((maxy / 2) - 3, (maxx / 2) - 18,
			"**********************************");
	mvprintw((maxy / 2) - 2, (maxx / 2) - 18,
			"*             PONG!              *");
	mvprintw((maxy / 2) - 1, (maxx / 2) - 18,
			"*By Matt Pike and John Schlederer*");
	mvprintw((maxy / 2), (maxx / 2) - 18, "*  For CS2852 with Dr. Schilling *");
	mvprintw((maxy / 2) + 1, (maxx / 2) - 18,
			"**********************************");
	refresh();
	usleep(2000000);
	clear();

	mvprintw(2, 4, "Player 1: %s", playerName);
	pthread_mutex_unlock(&drawlock);

	// Start the threads
	if ((rc1 = pthread_create(&thread1, NULL, &moveball, NULL))) {
		fprintf(stderr, "Ball movement thread creation failed.");
	}
	if ((rc2 = pthread_create(&thread2, NULL, &moveopponent, NULL))) {
		fprintf(stderr, "Oponent thread creation failed");
	}
	if ((rc3 = pthread_create(&thread3, NULL, &moveme, NULL))) {
		fprintf(stderr, "Player thread creation failed");
	}
	if ((rc4 = pthread_create(&thread4, NULL, &keeptime, NULL))) {
		fprintf(stderr, "Timer thread creation failed");
	}
	if ((rc5 = pthread_create(&thread5, NULL, &playmusic, NULL))) {
		fprintf(stderr, "Music thread creation failed");
	}
	if ((rc6 = pthread_create(&thread6, NULL, &drawthings, NULL))) {
		fprintf(stderr, "Screen drawer thread creation failed");
	}

	// Wait for the threads to exit
	pthread_join(thread1, NULL);
	pthread_join(thread2, NULL);
	pthread_join(thread3, NULL);
	pthread_join(thread4, NULL);
	pthread_join(thread5, NULL);
	pthread_join(thread6, NULL);

	//Close audio interface
	closeAudioInterface();

	// tear down the window
	delwin(win);
	endwin();
	refresh();

	// get out of here
	return 0;
}

/************************************************************************************
 * Method header:
 * Function name: drawthings
 * Function purpose: Draws the net periodically to account for overwritten characters
 * Function parameters:
 *                   void* vp - used to pass data into the function, not used presently
 * Function return value: NULL, as the function does not return any data
 ************************************************************************************/
void *drawthings(void *vp) {
	while (!quit) {
		while (paused) {
			usleep(1000);
		}
		pthread_mutex_lock(&drawlock);
		int maxx, maxy;
		getmaxyx(win, maxy, maxx);
		int y;
		for (y = 0; y < maxy; y++) {
			if (y % 2 == 0) {
				mvprintw(y, maxx / 2, "|");
			}
		}
		pthread_mutex_unlock(&drawlock);
		usleep(500000);

	}

	return NULL;
}
