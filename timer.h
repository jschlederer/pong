/**
 * timer.c
 * @author John Schlederer & Matthew Pike
 * CS 3841 011
 * Description: Counts time elapsed while game is played
 * @date 24 October 2016
 */

#ifndef TIMER_H
#define TIMER_H

#ifdef SRC_TIMER_C
#define EXTERN_PFX
#else
#define EXTERN_PFX extern
#endif

/************************************************************************************
 * External Includes
 ************************************************************************************/
#include <pthread.h>
#include <curses.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
/************************************************************************************
 * Public structure / type definitions
 ************************************************************************************/

/************************************************************************************
 * Public function / method prototypes
 ************************************************************************************/
/**
 * Counts how long the game has been running
 * @param vp This is a pointer to the parameters passed into the
 *           thread.  At the present time, this parameter is not used.
 */
EXTERN_PFX void *keeptime(void* vp);

#undef EXTERN_PFX
#endif
