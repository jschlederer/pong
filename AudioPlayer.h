/**
 * AudioPlayer.h
 * @author John Schlederer & Matthew Pike
 * CS 3841 011
 * Description: Plays continuous background music and a sound when a collision is detected.
 * @date 24 October 2016
 */

#ifndef SRC_AUDIOPLAYER_H_
#define SRC_AUDIOPLAYER_H_

#ifdef SRC_AUDIOPLAYER_C
#define EXTERN_PFX
#else
#define EXTERN_PFX extern
#endif

/************************************************************************************
 * External Includes
 ************************************************************************************/
#include <pthread.h>
#include <stdio.h>
#include <math.h>
#include "AudioInterface.h"
/************************************************************************************
 * Public structure / type definitions
 ************************************************************************************/

/************************************************************************************
 * Public function / method prototypes
 ************************************************************************************/

/**
 * Plays continuous music while the game is not paused
 * @param vp This is a pointer to the parameters passed into the
 *           thread.  At the present time, this parameter is not used.
 */
EXTERN_PFX void *playmusic(void* vp);

/**
 * Plays collision sound when collision was detected
 */
EXTERN_PFX void *playcollisionsound();

/**
 * Adjust the volume of the music being played as command line parameter
 * @param buffer The buffer to be manipulated
 * @param bufferLength Length of buffer
 * @param gain The gain factor to increase or decrease volume by
 * @return Return the adjusted buffer
 */
void *adjustGain(void *buffer, int bufferLength, int gain);

#undef EXTERN_PFX

#endif /* SRC_AUDIOPLAYER_H_ */
