/**
 * timer.c
 * @author John Schlederer & Matthew Pike
 * CS 3841 011
 * Description: Counts time elapsed while game is played
 * @date 24 October 2016
 */

#define SRC_TIMER_C

/************************************************************************************
 * External Includes
 ************************************************************************************/
#include "pong.h"
#include "timer.h"

/**
 * Counts how long the game has been running
 * @param vp This is a pointer to the parameters passed into the
 *           thread.  At the present time, this parameter is not used.
 */
void *keeptime(void *vp) {
	double elapsed;
	elapsed = 0;
	//Counts while game is not paused or quit
	while (!quit) {
		while (paused) {
			usleep(1000);
		}
		//Waits one second
		usleep(1000000);

		//Increases time by 1
		elapsed = elapsed + 1.0;

		//Draws out to window
		pthread_mutex_lock(&drawlock);
		mvprintw(1, 4, "Time elapsed: %.2f s", elapsed);
		pthread_mutex_unlock(&drawlock);

	}
	return NULL;
}
