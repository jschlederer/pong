/**
 * AudioPlayer.c
 * @author John Schlederer & Matthew Pike
 * CS 3841 011
 * Description: Plays continuous background music and a sound when a collision is detected.
 * @date 24 October 2016
 */

#define SRC_AUDIOPLAYER_C
#include "AudioPlayer.h"
#include "pong.h"

//Collision varible
bool collision;

/**
 * Plays continuous music while the game is not paused
 * @param vp This is a pointer to the parameters passed into the
 *           thread.  At the present time, this parameter is not used.
 */
void *playmusic(void *vp) {
	collision = false;
	char buffer[2048];

	int rc = 1;
	bool opened = false;

	//Opens file
	int fileDesc = open("StarWarsImerpialMarchRaw.raw", O_RDONLY); // = open("CantinaBandRaw.raw", O_RDONLY);

	//Opened set to true
	opened = true;
	while (!quit) {
		while (!paused && !quit) {
			//Read while not open
			if (!opened) {
				//File is open
				fileDesc = open("StarWarsImerpialMarchRaw.raw", O_RDONLY);
				opened = true;
			}

			//Read in file
			rc = read(fileDesc, buffer, sizeof(buffer));

			//If the file is not done being read
			if (rc > 0) {
				//Adjust sound by gain factor from command line parameter
				char *writeBuffer = adjustGain(&buffer[0], rc, volume);

				//Locks the mutex, plays the sound, unlocks mutex
				pthread_mutex_lock(&soundlock);
				writeToAudioInterface(writeBuffer, rc);
				pthread_mutex_unlock(&soundlock);

				//Clears both buffers
				memset(&buffer[0], 0, 2048);
				memset(writeBuffer, 0, 2048);
			} else {
				//If file is done opened is false
				opened = false;
			}
		}
	}
	return NULL;
}

/**
 * Plays collision sound when collision was detected
 */
void *playcollisionsound() {
	//Open collision sound file
	int fileDesc = open("beep.raw", O_RDONLY);
	int rc = 1;
	char buffer[2048];

	//Lock mutex, play collision sound, unlock mutex
	pthread_mutex_lock(&soundlock);
	do {
		rc = read(fileDesc, buffer, sizeof(buffer));
		if (rc > 0) {
			writeToAudioInterface(buffer, rc);
			memset(&buffer[0], 0, 2048);
		}
	} while (rc > 0);

	pthread_mutex_unlock(&soundlock);

	return NULL;
}

/**
 * Adjust the volume of the music being played as command line parameter
 * @param buffer The buffer to be manipulated
 * @param bufferLength Length of buffer
 * @param gain The gain factor to increase or decrease volume by
 * @return Return the adjusted buffer
 */
void *adjustGain(void *buffer, int bufferLength, int gain) {
	uint16_t *intBuffer = buffer;

	int i;

	//Multiples buffer by 2^gain
	for (i = 0; i < bufferLength; i++) {
		*(intBuffer + i) = *(intBuffer + i) * pow((double) 2, (double) gain);
	}
	return intBuffer;
}
