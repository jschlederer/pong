/************************************************************************************
 * This file was developed as part of CS3841 Design of Operating Systems at the
 * Milwaukee School of Engineering.  This file is copyright 2008-2014 by MSOE.
 *
 * $Author: wws $
 * $Revision: 1.1 $
 * $Name:  $
 * This file manages the motion of the ball.  This includes collisions, edge
 * detections, paddle detection, etc.
 *
 ************************************************************************************/
#define BALLCTRL_C

/************************************************************************************
 * External Includes
 ************************************************************************************/
#include <unistd.h>
#include "ballctrl.h"
#include "pong.h"
#include "AudioPlayer.h"

/************************************************************************************
 * Private structure / type definitions
 ************************************************************************************/
#define EASYDIF (100000)
#define HARDDIF (50000)
/************************************************************************************
 * Private function / method prototypes
 ************************************************************************************/
static void collision();

/************************************************************************************
 * Constant declarations / table declarations
 ***********************************************************************************/

/************************************************************************************
 * Method header:
 * Function name: moveball
 * Function purpose: This function is responsible for moving the ball within the game of pong.
 *                   It is spawned as a thread and will exit if and when quit is no longer true.
 * Function parameters: 
 *                   void *vp - This is a pointer to the parameters passed into the 
 *                              thread.  At the present time, this parameter is not used.
 * Function return value: void* This is the return value when the thread exits.  
 *                              Currently, it is always NULL, as no data is directly 
 *                              returned by the thread.
 ************************************************************************************/
// This thread is responsible for moving the ball
void *moveball(void* vp) {
	// get the extents of the screen
	int maxx;

	int maxy;

	int playerScore;
	int comScore;

	playerScore = 0;
	comScore = 0;

	getmaxyx(win, maxy, maxx);
	float yadder = 1.0f;
	float xadder = 1.0f;
	float xactual = 1.0f;
	float yactual = 1.0f;

	//Draws computer score and player score on screen
	pthread_mutex_lock(&drawlock);
	mvprintw(1, maxx - 19, "Computer Score: %d", comScore);
	mvprintw(3, 4, "Player Score: %d", playerScore);
	pthread_mutex_unlock(&drawlock);

	while (!quit) {
		while (paused) {
			usleep(1000);
		}
		//Draws the ball
		pthread_mutex_lock(&drawlock);
		mvaddch(bally, ballx, ' ' | A_NORMAL);
		pthread_mutex_unlock(&drawlock);
		yactual += yadder;
		xactual += xadder;

		// truncate
		bally = (int) (yactual);
		ballx = (int) (xactual);
		if (bally > maxy - 1) {
			yadder = -yadder;
			//Check collision on top of screen
			collision();
		}
		if (bally < 1) {
			yadder = -yadder;
			//Check collision on bottom of screen
			collision();
		}
		if (ballx > maxx - 1) {
			xadder = -xadder;
			//Checks collision for right side (computer paddle)
			collision();
		}
		if (ballx < 1) {
			xadder = -xadder;
			//Checks collision for left side (player paddle
			if (bally >= playervpos && bally < playervpos + 5) {
				//If collision play sound
				collision();
			} else {
				//If no collision then the computer score increases and waits one second to start playing again
				comScore++;
				pthread_mutex_lock(&drawlock);
				mvprintw(1, maxx - 19, "Computer Score: %d", comScore);
				pthread_mutex_unlock(&drawlock);
				usleep(1000000);
				yadder = 1.0f;
				xadder = 1.0f;
				xactual = 1.0f;
				yactual = 1.0f;
				ballx = 1;
				bally = 1;
			}
		}

		pthread_mutex_lock(&drawlock);
		mvaddch(bally, ballx, ' ' | A_REVERSE);
		pthread_mutex_unlock(&drawlock);

		//Change speed depending on difficulty
		(lvlDif == 1) ? (usleep(EASYDIF)) : (usleep(HARDDIF));

	}
	return NULL;
}

/************************************************************************************
 * Method header:
 * Function name: collision
 * Function purpose: This method will be invoked when a ball collision is detected. 
 * Function parameters: 
 *                   None
 * Function return value: None
 ************************************************************************************/
static void collision() {
	//Play collision sound
	playcollisionsound();
	return;
}
