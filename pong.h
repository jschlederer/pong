/************************************************************************************
* This file was developed as part of CS3841 Design of Operating Systems at the 
* Milwaukee School of Engineering.  This file is copyright 2008-2009 by MSOE.
* 
* $Author: wws $
* $Revision: 1.1 $
* $Name:  $
* This file servers as the interface header definition for the ball controller logic of the 
* pong game.
*
************************************************************************************/
#ifndef PONG_H
#define PONG_H

#ifdef PONG_C
#define EXTERN_PFX 
#else
#define EXTERN_PFX extern
#endif

/************************************************************************************
 * External Includes
 ************************************************************************************/
#include <curses.h>
#include <pthread.h>
#include <unistd.h>

/************************************************************************************
 * Public structure / type definitions
 ************************************************************************************/
// None as of right now.

/************************************************************************************
 * Public / global variable definitions
 ************************************************************************************/
// Global data - for inter-thread communication
EXTERN_PFX int playervpos;
EXTERN_PFX int ballx;
EXTERN_PFX int bally;
EXTERN_PFX WINDOW *win;	// the curses window
EXTERN_PFX bool quit;  // a flag to stop all threads
EXTERN_PFX bool paused; // a flag to determine if the game is paused
EXTERN_PFX int lvlDif; //difficulty level (1 = easy, 2 = hard) passed in as command line parameter
EXTERN_PFX char* playerName; // player name passed as command line arg
EXTERN_PFX pthread_mutex_t drawlock;
EXTERN_PFX pthread_mutex_t soundlock;
EXTERN_PFX int volume;

/************************************************************************************
 * Public function / method prototypes
 ************************************************************************************/
/**
 * Method header:
 * Function name: main
 * Function purpose: This is the main function for the program.
 * Function parameters:
 *                  @param int argc  - This is a count of the number of command line parameters
 *                               passed by the operating system.
 *                  @param char *vp[] - This is the array of strings which is passed to program.
 * Function return value:
 * 				    @return int - 0 will be returned upon normal exiting of the program.
 *                                A negative value will indicate an error.
 */
EXTERN_PFX int main(int argc, char* argv[]);
EXTERN_PFX void *drawthings(void *vp);
#undef EXTERN_PFX
#endif
