/************************************************************************************
 * This file was developed as part of CS3841 Design of Operating Systems at the
 * Milwaukee School of Engineering.  This file is copyright 2008-2014 by MSOE.
 *
 * $Author: wws $
 * $Revision: 1.1 $
 * $Name:  $
 * This file manages the motion of the users paddle.  It is responsible for
 * reading commands from the keyboard and processing them appropriately.
 *
 ************************************************************************************/
#define PADDLECTRL_C

/************************************************************************************
 * External Includes
 ************************************************************************************/
#include <curses.h>
#include "pong.h"
#include "paddlectrl.h"

/************************************************************************************
 * Private structure / type definitions
 ************************************************************************************/
#define PADDLE_SIZE (5)

/************************************************************************************
 * Private function / method prototypes
 ************************************************************************************/

/************************************************************************************
 * Constant declarations / table declarations
 ***********************************************************************************/

/************************************************************************************
 * Method header:
 * Function name: moveme
 * Function purpose: This function is responsible for moving the users paddle.
 *                   It is spawned as a thread and ewill exit if and when quit is no longer true.
 * Function parameters: 
 *                   void *vp - This is a pointer to the parameters passed into the 
 *                              thread.  At the present time, this parameter is not used.
 * Function return value: void* This is the return value when the thread exits.  
 *                              Currently, it is always NULL, as no data is directly 
 *                              returned by the thread.
 ************************************************************************************/
// Run the user's paddle
void *moveme(void* vp) {
	int ch;
	playervpos = 0;	// start at top row

	// get the extents of the screen
	int maxy;
	int maxx;
	getmaxyx(win, maxy, maxx);

	// draw the default paddle
	int i;
	for (i = 0; i < PADDLE_SIZE; i++) {
		pthread_mutex_lock(&drawlock);
		mvaddch(i, 0, ' ' | A_REVERSE);
		pthread_mutex_unlock(&drawlock);
	}

	while (!quit) {
		//Checks keyboard for key press
		ch = getch();
		switch (ch) {

		//If up arrow move paddle up
		case KEY_UP:
			if (!paused) {
				playervpos--;
				if (playervpos < 0) {
					playervpos = 0;
				}
				pthread_mutex_lock(&drawlock);
				mvaddch(playervpos, 0, ' ' | A_REVERSE);
				mvaddch(playervpos+PADDLE_SIZE, 0, ' ' | A_NORMAL);
				pthread_mutex_unlock(&drawlock);
			}
			break;

		//If down arrow move paddle down
		case KEY_DOWN:
			if (!paused) {
				playervpos++;
				if (playervpos > (maxy - PADDLE_SIZE)) {
					playervpos = (maxy - PADDLE_SIZE);
				}
				pthread_mutex_lock(&drawlock);
				mvaddch(playervpos - 1, 0, ' ' | A_NORMAL);
				mvaddch(playervpos+PADDLE_SIZE-1, 0, ' ' | A_REVERSE);
				pthread_mutex_unlock(&drawlock);
			}
			break;

		//If 'q' then quit the program
		case 'q':
			quit = true;
			break;

		//If 'p' then pause the game
		case 'p':
			paused = !paused;
			break;
		default:
			break;
		}
	}
	maxy = maxx;
	return NULL;
}

